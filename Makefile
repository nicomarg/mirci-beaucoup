CC=gcc
SRCDIR=src
OUTDIR=out
BINDIR=bin
CFLAGS=-Wall -Werror -Wextra -Wshadow -Ofast #-ansi -pedantic -pedantic-errors -D_XOPEN_SOURCE=500 \
	-D_XOPEN_SOURCE_EXTENDED=1 $(MACHINE)
LDLIBS=-pthread -lncurses
LIBRARIES=createSocket treap messages display communication protocol
LIBS=$(addprefix $(OUTDIR)/,$(addsuffix .o,$(LIBRARIES)))
DEBUGLIBS=$(addprefix $(OUTDIR)/,$(addsuffix D.o,$(LIBRARIES)))
BINARIES=MIRClient MIRClientDebug MIRCtest MIRCtesty
TARGETS=$(addprefix $(BINDIR)/,$(BINARIES))

all: $(TARGETS) 

$(OUTDIR)/%.o: $(SRCDIR)/%.c
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -I $(SRCDIR) -c $< -o $@


$(BINDIR)/MIRClient: $(LIBS) $(OUTDIR)/main.o
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) $(LIBS) $(OUTDIR)/main.o -o $@ $(LDLIBS)

$(BINDIR)/MIRCtest: $(LIBS) $(OUTDIR)/test2.o
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) $(LIBS) $(OUTDIR)/test2.o -o $@ $(LDLIBS)

$(BINDIR)/MIRCtesty: $(LIBS) $(OUTDIR)/test3.o
	$(CC) $(CFLAGS) $(LIBS) $(OUTDIR)/test3.o -o $@ $(LDLIBS)

debug:

$(OUTDIR)/%D.o: $(SRCDIR)/%.c
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -I $(SRCDIR) -D DEBUG -c $< -o $@

$(BINDIR)/MIRClientDebug: $(DEBUGLIBS) $(OUTDIR)/main.o
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) $(DEBUGLIBS) $(OUTDIR)/main.o -o $@ $(LDLIBS)


.PHONY : clean tar pdf

clean:
	rm -f $(OUTDIR)/* $(BINDIR)/*

tar:
	@mkdir -p jacob-margulies
	@cp Makefile jacob-margulies/
	@cp README.md jacob-margulies/
	@cp -r src/ jacob-margulies/
	tar -czvf jacob-margulies.tar.gz jacob-margulies/
	@rm -rf jacob-margulies

pdf:
	latexmk -cd -outdir=../ -pdf >/dev/null 2>/dev/null $(SRCDIR)/mirci-beaucoup.tex
	latexmk -c -outdir=./ >/dev/null 2>/dev/null $(SRCDIR)/mirci-beaucoup.tex

