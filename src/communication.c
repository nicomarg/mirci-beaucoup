#include "communication.h"


pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
char buf[1024];
char *buf_ptr = buf + 4;
char rcv_buf[1024];
time_t lasttlv;
bool closing = false;
int sock = 0;
int multicastSock = 0;
struct sockaddr_in6 addr;
socklen_t address_len = sizeof(addr);
struct sockaddr_storage rcv_addr;
socklen_t rcv_addr_len;

//Sends current message, not thread-safe
void flushmessage()
{
    if(buf_ptr - buf == 4) return; //Skip if no tlvs in the buffer
    short length = htons(buf_ptr - buf - 4);
    memcpy(buf + 2, &length, 2);
    length = buf_ptr-buf;
    if(sendto(sock, buf, length, 0,(struct sockaddr *) &addr, address_len)==-1)
    {
        perror("sendto");
        displayError("%d\n", errno);
    }
    memset(buf, 0, 1024);
    buf[0] = 95;
    buf[1] = 0;
    buf_ptr = buf + 4;
}

void queuetlv(char *tlv, int size)
{
    pthread_mutex_lock(&mutex);
    if(buf_ptr + size - buf >= 1024) flushmessage();
    memcpy(buf_ptr, tlv, size);
    buf_ptr += size;
    lasttlv = time(NULL);
    // flushmessage(); // Uncomment to disable message aggregation
    pthread_mutex_unlock(&mutex);
}

void sendtlv(struct in6_addr destaddr, unsigned short port, char *tlv, bool do_free)
{
    if (sock == 0)
    {
        displayError("tlv sent before initializing socket\n");
        return;
    }
    if(tlv == NULL) return;
    unsigned char size = *tlv ? tlv[1] + 2 : 1;
    NeighbourKey newkey = {destaddr, port};
    if (neighKeyEq(addrtokey(addr), newkey) == false)
    {
        pthread_mutex_lock(&mutex);
        flushmessage();
        pthread_mutex_unlock(&mutex);
        memset(&addr, 0, address_len);
        addr.sin6_family = AF_INET6;
        //addr.sin6_addr = destaddr;
        memcpy(&(addr.sin6_addr.s6_addr),&destaddr,sizeof(struct in6_addr));
		addr.sin6_port = htons(port);
    }
    queuetlv(tlv, size);
    if(do_free) free(tlv);
}

void* getmessage(void *data)
{
    (void) data;
    ssize_t length;
    while(!closing)
    {
		// puts("Ready for new message");
        rcv_addr_len = sizeof(struct sockaddr_storage);
        length = recvfrom(sock, rcv_buf, 1024, 0, (struct sockaddr *)&rcv_addr, &rcv_addr_len);
#ifdef DEBUG
        /*displayError("recvfrom address: ");
        for(unsigned int i = 0; i < rcv_addr_len; i++) displayError("%02x", *((char*) &rcv_addr + i));
        displayError("\n");*/
#endif
        if(length == -1) continue;
        short msglen;
        memcpy(&msglen,rcv_buf + 2,2);
        msglen = ntohs(msglen);
        if (msglen + 4 != length)
        {
#ifdef DEBUG
            displayError("Message with wrong length received");
#endif
            sendtlv(((struct sockaddr_in6*)&rcv_addr)->sin6_addr,ntohs(((struct sockaddr_in6*)&rcv_addr)->sin6_port),writeWarning("Message with wrong length received from you !"),true); 
            continue;
        }
        readMessage(rcv_buf, (struct sockaddr_in6*) &rcv_addr);
    }
    return NULL;
}

void* regularflush(void *data)
{
    (void) data;
    while (!closing)
    {
		sleep(1);
		pthread_mutex_lock(&mutex);
        flushmessage();
		pthread_mutex_unlock(&mutex);
    }
    return NULL;
}

void sendHelloMulticast()
{
    char buffer[14];
    memcpy(buffer+4, writeShortHello(id), 10);
    buffer[0] = 95; buffer[1] = 0;
    unsigned short len = htons(10);
    memcpy(buffer + 2, &len, 2);
    struct sockaddr_in6 multiaddr;
    memset(&multiaddr, 0, sizeof(multiaddr));
    multiaddr.sin6_family = AF_INET6;
    multiaddr.sin6_port = htons(1212);
    inet_pton(AF_INET6, "ff02::4242:4242", &multiaddr.sin6_addr);
    if(sendto(multicastSock, buffer, 14, 0, (struct sockaddr *)&multiaddr, sizeof(multiaddr)) == -1)
        perror("sendto");
#ifdef DEBUG
    displayError("Sent Multicast Hello\n");
#endif
}

void *getMessageMulticast(void *data)
{
    (void) data;
    char buffer[1024];
    struct sockaddr_in6 rcvaddr;
    socklen_t len;
    ssize_t length;
    while(!closing)
    {
        len = sizeof(struct sockaddr_in6);
        length = recvfrom(multicastSock, buffer, 1024, 0, (struct sockaddr *)&rcvaddr, &len);
#ifdef DEBUG
        displayError("Received multicast\n");
#endif
        if(length == -1) continue;
        short msglen;
        memcpy(&msglen,buffer + 2,2);
        msglen = ntohs(msglen);
        if (msglen + 4 != length)
        {
#ifdef DEBUG
            displayError("Message with wrong length received");
#endif
        }
        readMessage(buffer, (struct sockaddr_in6*) &rcvaddr);
    }
    return NULL;
}

void initMulticast()
{
    if((multicastSock = socket(AF_INET6, SOCK_DGRAM, 0)) == -1)
        perror("socket");
    int val = 1;
    setsockopt(multicastSock, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
    setsockopt(multicastSock, IPPROTO_IPV6, IPV6_V6ONLY, &val, sizeof(val));
    setsockopt(multicastSock, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, &val, sizeof(val));
    setsockopt(multicastSock, IPPROTO_IPV6, IPV6_UNICAST_HOPS, &val, sizeof(val));
    val = 0;
    setsockopt(multicastSock, IPPROTO_IPV6, IPV6_MULTICAST_LOOP, &val, sizeof(val));
    struct sockaddr_in6 multicastaddr;
    memset(&multicastaddr, 0, sizeof(multicastaddr));
    multicastaddr.sin6_family = AF_INET6;
    multicastaddr.sin6_addr = in6addr_any;
    multicastaddr.sin6_port = htons(1212);
    if(bind(multicastSock, (struct sockaddr *)&multicastaddr, sizeof(multicastaddr)) == -1)
        perror("bind");
    struct ipv6_mreq mreq;
    memset(&mreq, 0, sizeof(mreq));
    inet_pton(AF_INET6, "ff02::4242:4242", &mreq.ipv6mr_multiaddr);
    // mreq.ipv6mr_interface = if_nametoindex("lo"); //leave 0 for default interface
    setsockopt(multicastSock, IPPROTO_IPV6, IPV6_JOIN_GROUP, &mreq, sizeof(mreq));
    displayError("Multicast started\n");
    pthread_t thread;
    pthread_create(&thread, NULL, getMessageMulticast, NULL);
}

int initSocket(int port, bool multicast)
{
    memset(buf, 0, 1024);
    buf[0] = 95;
    buf[1] = 0;
	memset(&addr,0,address_len);
    addr.sin6_family = AF_INET6;
    memset(&rcv_addr, 0, sizeof(rcv_addr));
    rcv_addr.ss_family = AF_INET6;
    sock = createSocket(AF_INET6, SOCK_DGRAM, port);
    if(multicast) initMulticast();
    pthread_t thread, thread2;
    pthread_create(&thread, NULL, regularflush, NULL);
    pthread_create(&thread2, NULL, getmessage, NULL);
    return sock;
}

void closeSocket()
{
    closing = true;
    sleep(1);
    close(sock);
}

