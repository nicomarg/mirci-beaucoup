#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include "messages.h"
#include <pthread.h>
#include "createSocket.h"
#include <stdbool.h>
#include "treap.h"
#include <time.h>
#include <errno.h>

int initSocket(int, bool);
void closeSocket(void);
void sendtlv(struct in6_addr, unsigned short, char*, bool);
void sendHelloMulticast(void);

#endif
