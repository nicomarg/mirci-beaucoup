/**
 * Function for creation and binding of a socket 
 *
 */

#include "createSocket.h"

int createSocket(int domain, int type, int port)
{
    int s;
    socklen_t len;
    s = socket(domain, type, 0);
    if(s == -1)
    {
        perror("Create socket");
        return -1;
    }
    int val = 1;
    if(setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val)) == -1)
        perror("setsockopt");
    if(domain == AF_INET)
    {
        struct sockaddr_in addr;
        len = sizeof(addr);
        memset(&addr, 0, len);
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        if(bind(s, (struct sockaddr *)&addr, len) == -1)
        {
            perror("bind");
            return -1;
        }
    }
    if(domain == AF_INET6)
    {
        struct sockaddr_in6 addrv6;
        len = sizeof(addrv6);
        memset(&addrv6, 0, len);
        addrv6.sin6_family = AF_INET6;
        addrv6.sin6_port = htons(port);
        if(bind(s, (struct sockaddr *)&addrv6, len) == -1)
        {
            perror("bind");
            return -1;
        }
    }
    return s;
}
