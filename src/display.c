#include "display.h"

WINDOW *win, *inputpad;
int maxrow, maxcol;
char message[241];
char nick[11];
char sendingBuffer[256];
int cursor, beg, window_size, msg_end;
bool displayClosing = false;
bool displayEnabled = false;
pthread_mutex_t mutexDisplay = PTHREAD_MUTEX_INITIALIZER;
int displaytype;


void closeDisplay(int sig_num)
{
    (void) sig_num;
	if(displaytype == 1)
	{
		displayClosing = true;
		stopProtocol();
		if(displayEnabled)
        {
			pthread_mutex_lock(&mutexDisplay);
            nocbreak();
            echo();
            keypad(win, FALSE);
            leaveok(win, FALSE);
            nodelay(win, FALSE);
            endwin();
			displayEnabled = false;
        }
		displayClosing = false;
	}
	else if(displaytype == 2)
	{
		stopProtocol();
	}
}

void displayInput()
{
    pthread_mutex_lock(&mutexDisplay);
	int oldmaxrow=maxrow;
    getmaxyx(win, maxrow, maxcol);
	if(oldmaxrow<maxrow)
	{
		move(0,0);
		for(int i=0;i<maxrow-oldmaxrow;i++)
			insertln();
	}
	else
	{
		move(0,0);
		for(int i=0;i<oldmaxrow-maxrow;i++)
			deleteln();
	}
	refresh();
	window_size = (242 < maxcol?242:maxcol-1);
    prefresh(inputpad, 0, beg, maxrow - 2, 0, maxrow-1, window_size);
	move(maxrow - 1, cursor - beg);
	refresh();
    pthread_mutex_unlock(&mutexDisplay);
}

char addName[100],addPort[10];

void interpretMessage()
{
    if(!strncmp(message, "/quit", 5)) closeDisplay(0);
	else if(!strncmp(message, "/help",5)) {
		displayOwnMessage("/help -> Affiche les commandes\n");
		displayOwnMessage("/quit -> Ferme le client\n");
		displayOwnMessage("/neighbour <adresse> <port> -> Ajoute un voisin\n");
	}
	else if(!strncmp(message, "/tvp", 4)) printVP(TVP);
	else if(!strncmp(message, "/tva", 4)) printVA(TVA);
	else if(!strncmp(message, "/neighbour", 10)) {
		struct addrinfo info;
		memset(&info, 0, sizeof(info));
		info.ai_socktype = SOCK_DGRAM;
		info.ai_protocol = 0;
		info.ai_flags = AI_V4MAPPED | AI_ALL;
		struct addrinfo *res;
		sscanf(message, "/neighbour %s%s",addName, addPort);
#ifdef DEBUG
		displayError("Tente d'ajouter %s %s\n",addName,addPort);
#endif
		int ret = getaddrinfo(addName, addPort, &info, &res);
		if(ret!=0)
			displayError("Adresse incorrecte\n");
		else
		{
			struct sockaddr_in6 a;
			memset(&a, 0, sizeof(a));
			memcpy(&a, res->ai_addr, res->ai_addrlen);
			freeaddrinfo(res);
			a.sin6_family = AF_INET6;

			TVP = insertVP(TVP, createVP(a.sin6_addr, ntohs(a.sin6_port)));
			
			char* tlv = writeShortHello(id);
			sendtlv(a.sin6_addr,ntohs(a.sin6_port),tlv,true);
		}
	}
    else {
		strcpy(sendingBuffer,nick);
		int nsz = strlen(nick);
		int msz = strlen(message);
		memcpy(sendingBuffer+nsz-1," : ",3);
		memcpy(sendingBuffer+nsz+2,message,msz-1);
		sendingBuffer[nsz+msz+1]='\0';
		displayOwnMessage("%.*s\n",nsz+msz+1,sendingBuffer);
		sendMessage(sendingBuffer);
	}
}

void handle_key_pressed(int key)
{
	(void) key;
    pthread_mutex_lock(&mutexDisplay);
	window_size = (242 < maxcol?242:maxcol-1);
    switch(key)
    {
        case KEY_BACKSPACE:
            if(cursor > 0)
            {
                cursor--;
                mvwdelch(inputpad, 1, cursor);
                msg_end--;
                if(cursor < beg)
                {
                    beg--;
                }
            }
            break;
        case KEY_DC:
            mvwdelch(inputpad, 1, cursor);
            msg_end--;
            break;
        case KEY_LEFT:
            if(cursor > 0)
            {
                cursor--;
                if(cursor < beg)
                    beg--;
            }
            break;
        case KEY_RIGHT:
            if(cursor < 241 && cursor < msg_end)
            {
                cursor++;
                if(cursor > beg + window_size)
                    beg++;
            }
            break;
        case KEY_HOME:
            cursor = 0;
            beg = 0;
            break;
        case KEY_END:
            cursor = msg_end;
            if(cursor > beg + window_size)
                beg = cursor - window_size;
            break;
        case '\n':
            mvwinsch(inputpad, 1, msg_end, '\n');
            msg_end++; cursor++;
            mvwinnstr(inputpad, 1, 0, message, msg_end);
            message[msg_end] = '\0';
            pthread_mutex_unlock(&mutexDisplay);
            interpretMessage();
            pthread_mutex_lock(&mutexDisplay);
            memset(message, 0, 241);
            wmove(inputpad, 1, 0);
            for(int i = 0;i <=window_size; i++) waddch(inputpad, ' ');
            cursor = beg = msg_end = 0;
            break;
        default:
            if(msg_end >= 240) beep();
            else if(key <= 256)
            {
                mvwinsch(inputpad, 1, cursor, key);
                msg_end++; cursor++;
                if (cursor > beg + window_size) beg++;
            }
    }
    pthread_mutex_unlock(&mutexDisplay);
}

void* readInput(void *data)
{
	(void) data;
    int key;
	(void) key;
	if(displaytype==2) puts("You can start writing...");
    while(!displayClosing)
    {
		if(displaytype==1)
		{
			displayInput();
			key = mvgetch(maxrow-1,cursor-beg);
			if(key != ERR) handle_key_pressed(key);
		}
		else if(displaytype==2)
		{
			char* ret = fgets(message,230,stdin);
			if(ret!=NULL)
			{
				interpretMessage();
			}
			else
			{
				break;
			}
		}
    }
    closeDisplay(0);
    return NULL;
}

void initDisplay(int type)
{
	displaytype=type;
	if(type==1)
	{
		puts("Please enter your nickname (10 characters at most)");
		while(!fgets(nick,11,stdin));
		win = initscr();
		cbreak();
		noecho();
		keypad(win, TRUE);
		start_color();
        init_color(COLOR_WHITE, 1000, 1000, 1000);
		init_pair(1, COLOR_GREEN, COLOR_BLACK);
		init_pair(2, COLOR_RED, COLOR_BLACK);
		init_pair(3, COLOR_BLUE, COLOR_BLACK);
		getmaxyx(win, maxrow, maxcol);

		inputpad = newpad(2, 242);
		mvwhline(inputpad, 0, 0, 0, 242);
		memset(message, 0, 241);
		beg = cursor = msg_end = 0;
		window_size = (242 < maxcol?242:maxcol-1);

		signal(SIGINT, closeDisplay);
		displayEnabled = true;
		displayInput();
		pthread_t thread;
		pthread_create(&thread, NULL, readInput, NULL);
	}
	else if(type==2)
	{
		puts("Please enter your nickname (10 characters at most)");
		while(!fgets(nick,11,stdin));
		pthread_t thread;
		pthread_create(&thread, NULL, readInput, NULL);
	}
}

char displayBuffer[1000];

void addMessage(const char *format, va_list args)
{
	int len=vsprintf(displayBuffer, format, args);
    pthread_mutex_lock(&mutexDisplay);
    getmaxyx(win, maxrow, maxcol);
	for(int i=0;i<=len/maxcol;i++)
	{
    	move(0,0);
    	deleteln();
   		move(maxrow - 4, 0);
    	insertln();
   		move(maxrow - 4, 0);
    	wprintw(win, "%.*s",maxcol,displayBuffer+i*maxcol);
	}
    pthread_mutex_unlock(&mutexDisplay);
	displayInput();
}

void displayMessage(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    
    if(displayEnabled)
	{
		attron(COLOR_PAIR(1));
		addMessage(format, args);
		attron(COLOR_PAIR(0));
    }
	else
    {
		printf("\e[1;32m");
        vprintf(format, args);
		printf("\e[0m");
        fflush(stdout);
    }

    va_end(args);
}

void displayError(const char *format, ...)
{
    va_list args;
    va_start(args, format);

    if(displayEnabled)
    {
        attron(COLOR_PAIR(2));
        addMessage(format, args);
        attron(COLOR_PAIR(0));
    }
    else
    {
		fprintf(stderr,"\e[1;31m");
        vfprintf(stderr, format, args);
		fprintf(stderr,"\e[0m");
        fflush(stderr);
    }

    va_end(args);
}

void displayOwnMessage(const char *format, ...)
{
	va_list args;
	va_start(args, format);
	if(displayEnabled)
	{
		attron(COLOR_PAIR(3));
		addMessage(format,args);
		attron(COLOR_PAIR(0));
	}
	else
	{
		printf("\e[1;34m");
		vprintf(format,args);
		printf("\e[0m");
		fflush(stdout);
	}
	va_end(args);
}
