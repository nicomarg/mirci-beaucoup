#ifndef DISPLAY_H
#define DISPLAY_H

#include <signal.h>
#include <stdarg.h>
#include <curses.h>
#include "protocol.h"

extern bool displayClosing;
void closeDisplay(int);
void initDisplay(int);
void displayMessage(const char*, ...);
void displayError(const char*, ...);
void displayOwnMessage(const char*, ...);

#endif
