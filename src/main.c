#include "protocol.h"

int main(int argc, char* argv[])
{
	printf("Expected parameter format : <address> <port> [mc]\n \"mc\" enables multicast\n");
	struct sockaddr_in6 a;
	memset(&a, 0, sizeof(a));
    struct addrinfo info;
    memset(&info, 0, sizeof(info));
    info.ai_socktype = SOCK_DGRAM;
    info.ai_protocol = 0;
    info.ai_flags = AI_V4MAPPED | AI_ALL;
    struct addrinfo *res;
    int ret = getaddrinfo(argv[1], argv[2] , &info, &res);
	if(ret!=0)
		fprintf(stderr,"Incorrect address\n");
	else
	{
		memcpy(&a, res->ai_addr, res->ai_addrlen);
		freeaddrinfo(res);
		a.sin6_family = AF_INET6;

    	TVP = insertVP(TVP, createVP(a.sin6_addr, ntohs(a.sin6_port)));
	}
	bool multicastOption = false;
	if(argc>3 && !strcmp(argv[3],"mc"))
		multicastOption=true;
	srand(time(NULL));
	int	port = (rand() % (1<<15)) + 1024;
	runProtocol(port, 1, multicastOption);
}
