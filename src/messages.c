#include "messages.h"

// Lecture de TLV (tag,length,value)

char* readTLV(char* message, struct sockaddr_in6 *from, int max_size)
{
	unsigned char type = *message;
	++message;
	if(type==0) {
		// Pad1 -> ignored
		return message;
	}
	unsigned char length = *message;
	++message;
	char* end = message + length;
    if(length > max_size)
    {
#ifdef DEBUG
        displayError("Dropped overflowing TLV\n");
#endif
		sendtlv(from->sin6_addr,ntohs(from->sin6_port),writeWarning("TLV was too long for message length !"),true);
        return message + max_size;
    }
	if(type==1) { // PadN, nothing to do except ignoring length bytes
		
	}
	else if(type==2) { // Hello
		if(length==8) { // format court
			// lire sourceId sur 8 octets
			unsigned long long sourceId=0;
			memcpy(&sourceId,message,8);
			if(sourceId!=id)
            	shortHello(sourceId, from);
		}
		else if(length==16) { // format long
#ifdef DEBUG
			displayError("Received long Hello\n");
#endif
			// lire sourceId sur 8 octets
			unsigned long long sourceId=0;
			memcpy(&sourceId,message,8);
			// lire destinationId sur 8 octets
			unsigned long long destinationId=0;
			memcpy(&destinationId,message+8,8);
            if(sourceId!=id)
                longHello(sourceId, destinationId, from);
		}
		else { // invalide
#ifdef DEBUG
			displayError("Unexpected Hello length\n");
#endif
			sendtlv(from->sin6_addr,ntohs(from->sin6_port),writeWarning("Hello TLV had unexpected length !"),true);
		}
	}
	else if(type==3) { // Neighbour
		if(length!=18) {
#ifdef DEBUG
			displayError("Unexpected Neighbour length\n");
#endif
			sendtlv(from->sin6_addr,ntohs(from->sin6_port),writeWarning("Neighbour TLV had unexpected length !"),true);
		}
		else if(!isSymmetrical(addrtokey(*from))) {
#ifdef DEBUG
			displayError("Received Neighbour from non-symmetrical neighbour\n");
#endif
			sendtlv(from->sin6_addr,ntohs(from->sin6_port),writeWarning("Neighbour TLV sent but you are not a symmetrical neighbour !"),true);

		}
		else
		{
			// lire addrIP sur 16 octets
			struct in6_addr addrIP;
			memcpy(&addrIP, message, 16);
			// lire port sur 2 octets
			short port;
			memcpy(&port,message+16,2);
			port = ntohs(port);
			NeighbourKey key = {addrIP, port};
			pthread_mutex_lock(&mutexVA);
			if(findVP(TVP, key) == NULL) TVP = insertVP(TVP, createVP(key.addrIP, key.port));
			pthread_mutex_unlock(&mutexVA);
		}
	}
	else if(type==4) { // Data
		pthread_mutex_lock(&mutexVA);
		bool symTest=isSymmetrical(addrtokey(*from));
		pthread_mutex_unlock(&mutexVA);
		if(!symTest)
		{
#ifdef DEBUG
			displayError("Received Data from non-symmetrical neighbour\n");
#endif
			sendtlv(from->sin6_addr,ntohs(from->sin6_port),writeWarning("Data TLV sent but you are not a symmetrical neighbour !"),true);
		}
		else
		{
			// lire senderId sur 8 octets
			unsigned long long senderId=0;
			memcpy(&senderId,message,8);
			// lire nonce sur 4 octets
			unsigned long nonce=0;
			memcpy(&nonce,message+8,4);
			// envoyer un Ack
			sendtlv(from->sin6_addr, ntohs(from->sin6_port), writeAck(senderId, nonce), true);
			// lire data sur le reste du message
			pthread_mutex_lock(&mutexData);
			DataKey key = {senderId, nonce};
			NodeRD* dataNode = findRD(TRD, key);
			if(dataNode == NULL && senderId != id)
			{
				char *data = malloc(length + 2);
				memcpy(data, message - 2, length + 2);
				dataNode = createRD(senderId, nonce, data);
				pthread_mutex_lock(&mutexVA);
				dataNode->neighbourList = addVS(TVA, time(NULL), NULL);
				pthread_mutex_unlock(&mutexVA);
				TRD = insertRD(TRD, dataNode);
				displayMessage("%.*s\n", length - 12, message + 12);
			}
			dataNode->neighbourList = removeNL(dataNode->neighbourList, addrtokey(*from));
			pthread_mutex_unlock(&mutexData);
		}
	}
	else if(type==5) { // Ack
		// lire senderId sur 8 octets
		unsigned long long senderId=0;
		memcpy(&senderId,message,8);
		// lire nonce sur 4 octets
		unsigned long nonce=0;
		memcpy(&nonce,message+8,4);
        DataKey key = {senderId, nonce};
		// displayError("Received Ack for %lu\n",nonce);
        pthread_mutex_lock(&mutexData);
        NodeRD* dataNode = findRD(TRD, key);
        if(dataNode != NULL)
		{
            dataNode->neighbourList = removeNL(dataNode->neighbourList, addrtokey(*from));
		}
        pthread_mutex_unlock(&mutexData);
	}
	else if(type==6) { // GoAway
		// lire code sur 1 octet
		unsigned char code;
		memcpy(&code,message,1);
		// lire message (codé en utf8)
		displayError("Go Away, code %d : %.*s\n", code, length - 2, message + 1);
        	pthread_mutex_lock(&mutexVA);
		NodeVA* neighbour = findVA(TVA,addrtokey(*from));
		displayError("Removed neighbour : %hu, %llu\n",neighbour->key.port,neighbour->id);
        	TVA = removeVA(TVA, addrtokey(*from));
		pthread_mutex_unlock(&mutexVA);
	}
	else if(type==7) { // Warning
		// lire message (codé en utf8)
#ifdef DEBUG
        	displayError("Warning : %.*s\n", length - 2, message);
#endif
	}
	else {
#ifdef DEBUG
		displayError("Unexpected type for TLV\n");
#endif
	}
	// passer les informations ignorées
	return end;
}


void readMessage(char* message, struct sockaddr_in6 *from)
{
	if(message[0] != 95 || message[1] != 0) {
#ifdef DEBUG
		displayError("Wrong magic number\n");
#endif
		return;
	}
	message+=2;
	unsigned short length;
	memcpy(&length,message,2);
    message += 2;
	length = ntohs(length);
	if(length>1024) {
#ifdef DEBUG
		displayError("Message is too long\n");
#endif
	}
	length=(1024<length ? 1024 : length);
	char* end=message+length;
	while(message<end)
		message = readTLV(message, from, end - message);
}


char* writePadN(unsigned char length) 
{
	char* tlv = malloc(length+2);
	tlv[0]=1;
	tlv[1]=length;
    memset(tlv + 2, 0, length);
	return tlv;
}

char* writeShortHello(unsigned long long sourceId)
{
	char* tlv = malloc(10);
	tlv[0]=2;
	tlv[1]=8;
	memcpy(tlv+2,&sourceId,8);
	return tlv;
}

char* writeLongHello(unsigned long long sourceId, unsigned long long destId)
{
	char* tlv = malloc(18);
	tlv[0]=2;
	tlv[1]=16;
	memcpy(tlv+2,&sourceId,8);
	memcpy(tlv+10,&destId,8);
    return tlv;
}

char* writeNeighbour(struct in6_addr addr, short port)
{
	char* tlv = malloc(20);
	tlv[0]=3;
	tlv[1]=18;
	memcpy(tlv+2,&addr.s6_addr,16);
	port = htons(port);
	memcpy(tlv+18,&port,2);
    return tlv;
}

char* writeData(unsigned long long senderId, unsigned int nonce, char* data)
{
    int sz = strlen(data) + 1;
	char* tlv = malloc(sz+14);
	tlv[0]=4;
	tlv[1]=(unsigned char)(sz+12);
	memcpy(tlv+2,&senderId,8);
	memcpy(tlv+10,&nonce,4);
	strcpy(tlv+14,data);
	return tlv;
}

char* writeAck(unsigned long long senderId,unsigned int nonce)
{
	char* tlv = malloc(14);
	tlv[0]=5;
	tlv[1]=12;
	memcpy(tlv+2,&senderId,8);
	memcpy(tlv+10,&nonce,4);
	return tlv;
}

char* writeGoAway(char code, char* explanation)
{
    int sz = strlen(explanation) + 1;
	char* tlv = malloc(3+sz);
	tlv[0]=6;
	tlv[1]=sz+1;
	tlv[2]=code;
	strcpy(tlv+3,explanation);
	return tlv;
}

char* writeWarning(char* warning)
{
    int sz = strlen(warning) + 1;
	char* tlv = malloc(sz+2);
	tlv[0]=7;
	tlv[1]=sz;
	strcpy(tlv+2,warning);
	return tlv;
}
