#ifndef MESSAGES_H
#define MESSAGES_H

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <string.h>
#include "protocol.h"

char* readTLV(char*, struct sockaddr_in6*, int);

void readMessage(char*, struct sockaddr_in6*);

char* writePadN(unsigned char);

char* writeShortHello(unsigned long long);

char* writeLongHello(unsigned long long, unsigned long long);

char* writeNeighbour(struct in6_addr, short);

char* writeData(unsigned long long, unsigned int, char*);

char* writeAck(unsigned long long, unsigned int);

char* writeGoAway(char, char*);

char* writeWarning(char*);

#endif
