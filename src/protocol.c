#include "protocol.h"

#define minNS 4
#define seedMask 0x2942294229422942

pthread_mutex_t mutexVA = PTHREAD_MUTEX_INITIALIZER;
NodeVP* TVP = NULL;
NodeVA* TVA = NULL;
pthread_mutex_t mutexData = PTHREAD_MUTEX_INITIALIZER;
NodeRD* TRD = NULL;
unsigned long long id;
unsigned int nonce = 0;
bool protocol_running;

NodeVA* Hello(unsigned long long sourceId, struct sockaddr_in6* from)
{
    NeighbourKey val = addrtokey(*from);
    if (findVP(TVP, val) == NULL) TVP = insertVP(TVP, createVP(val.addrIP, val.port));
    NodeVA* neighbour = findVA(TVA, val);
    if (neighbour == NULL)
    {
	if(ntohs(from->sin6_port)!=1212) displayError("Found active neighbour : %hu, %llu\n",ntohs(from->sin6_port),sourceId);
        neighbour = createVA(val.addrIP, val.port);
        neighbour->lastlongHello = 0;
        TVA = insertVA(TVA, neighbour);
    }
    neighbour->id = sourceId;
    neighbour->lastHello = time(NULL);
    if(!isSymmetrical(val))sendtlv(neighbour->key.addrIP, neighbour->key.port, writeLongHello(id, sourceId), true);
    return neighbour;
}

void shortHello(unsigned long long sourceId, struct sockaddr_in6* from)
{
    // displayError("ShortHello : %llu\n", sourceId);
    pthread_mutex_lock(&mutexVA);
    Hello(sourceId, from);
    pthread_mutex_unlock(&mutexVA);
}

void longHello(unsigned long long sourceId, unsigned long long destinationId,
        struct sockaddr_in6* from)
{
    // displayError("LongHello : %llu, %llu\n", sourceId, destinationId);
    pthread_mutex_lock(&mutexVA);
	NeighbourKey val = addrtokey(*from);
	if(val.port != 1212)
	{
		val.port = 1212;
		NodeVA* multicastAlias = findVA(TVA,val);
		if(multicastAlias != NULL && multicastAlias->id == sourceId)
			TVA = removeVA(TVA,val);
	}
    NodeVA* neighbour = Hello(sourceId, from);
    if (destinationId == id)
        neighbour->lastlongHello = time(NULL);
    pthread_mutex_unlock(&mutexVA);
}

NodeVA* cleanupNeighbours(NodeVA *neighbour)
{
    if(neighbour == NULL) return neighbour;
    else if(time(NULL) - neighbour->lastHello >= 120)
    {
	displayError("Removed inactive neighbour : %hu, %llu\n",neighbour->key.port,neighbour->id);
        char *message = "No hello for more than 2 min.";
        char *tlv = writeGoAway(2, message);
        sendtlv(neighbour->key.addrIP, neighbour->key.port, tlv, true);
        NodeVA *uleft = neighbour->left, *uright = neighbour->right;
        free(neighbour);
        neighbour = mergeVA(uleft, uright);
        return cleanupNeighbours(neighbour);
    }
    neighbour->left = cleanupNeighbours(neighbour->left);
    neighbour->right = cleanupNeighbours(neighbour->right);
    return neighbour;
}

int sendLongHellos(NodeVA *neighbour)
{
    if(neighbour == NULL) return 0;
    char *tlv = writeLongHello(id, neighbour->id);
    sendtlv(neighbour->key.addrIP, neighbour->key.port, tlv, true);
    int val = (time(NULL) - neighbour->lastlongHello < 120);
    return val + sendLongHellos(neighbour->left) + sendLongHellos(neighbour->right);
}

NodeNL* floodNeighbours(NodeNL* neighbour, char* tlv, time_t now)
{
    if(neighbour == NULL) return neighbour;
    if(now - neighbour->lastSend - neighbour->waitingTime >= 0)
    {
        sendtlv(neighbour->key.addrIP, neighbour->key.port, tlv, false);
        neighbour->cnt++;
        neighbour->lastSend = now;
        neighbour->waitingTime = (rand() % (1<<(neighbour->cnt - 1))) + (1<<(neighbour->cnt - 1));
        if(neighbour->cnt >= 5)
        {
            char *message = "No Ack after 5 sends.";
            char *tlv2 = writeGoAway(2, message);
            sendtlv(neighbour->key.addrIP, neighbour->key.port, tlv2, true);
            pthread_mutex_lock(&mutexVA);
#ifdef DEBUG
            NodeVA* tmpneigh = findVA(TVA, neighbour->key);
	    displayError("Removed neighbour for no Ack : %hu, %llu\n",tmpneigh->key.port,tmpneigh->id);
#endif
            TVA = removeVA(TVA, neighbour->key);
            pthread_mutex_unlock(&mutexVA);
            NodeNL *uleft = neighbour->left, *uright = neighbour->right;
            free(neighbour);
            neighbour = mergeNL(uleft, uright);
            return floodNeighbours(neighbour, tlv, now);
        }
    }
    neighbour->left = floodNeighbours(neighbour->left, tlv, now);
    neighbour->right = floodNeighbours(neighbour->right, tlv, now);
    return neighbour;
}

NodeRD* floodData(NodeRD* data, time_t now)
{
    if(data == NULL) return data;
    if(now - data->date >= 360)
    {
#ifdef DEBUG
	displayError("DATA DELETION\n");
#endif
        NodeRD *uleft = data->left, *uright = data->right;
        destroyNL(data->neighbourList);
        free(data->data);
        free(data);
        data = mergeRD(uleft, uright);
        return floodData(data, now);
    }
    else 
	{
		data->neighbourList = floodNeighbours(data->neighbourList, data->data, now);
    }
	data->left = floodData(data->left, now);
    data->right = floodData(data->right, now);
    return data;
}

void sendMessage(char* message)
{
#ifdef DEBUG
    displayError("Sending message.\n");
#endif
    int length = strlen(message) + 1;
	fflush(stdout);
    if(length >= 242)
    {
        displayError("Message too long\n");
        return;
    }
    char *tlv = writeData(id, nonce, message);
    pthread_mutex_lock(&mutexVA);
    NodeRD *dataNode = createRD(id, nonce, tlv);
	nonce++;
    dataNode->neighbourList = addVS(TVA, time(NULL), NULL);
    pthread_mutex_unlock(&mutexVA);
    pthread_mutex_lock(&mutexData);
    TRD = insertRD(TRD, dataNode);
    pthread_mutex_unlock(&mutexData);
#ifdef DEBUG
    displayError("Sending complete\n");
#endif
}

void sendGoAway(NodeNL* u,char* tlv)
{
	if(u==NULL) return;
	sendtlv(u->key.addrIP,u->key.port,tlv,false);
	sendGoAway(u->left,tlv);
	sendGoAway(u->right,tlv);
}

void stopProtocol()
{
	protocol_running = false;
}

void sendNeighbourList(NodeNL* u, NodeNL* v)
{
	if(u==NULL) return;
	if(u!=v)
	{
		char* tlv = writeNeighbour(v->key.addrIP,v->key.port);
		sendtlv(u->key.addrIP,u->key.port, tlv, true);
	}
	sendNeighbourList(u->left,v);
	sendNeighbourList(u->right,v);
}

void propagateNeighbour(NodeNL* u, NodeNL* l)
{
	if(u==NULL) return;
	sendNeighbourList(l,u);
	propagateNeighbour(u->left,l);
	propagateNeighbour(u->right,l);
}

bool isSymmetrical(NeighbourKey key)
{
	NodeVA* neighbour = findVA(TVA,key);
	if(neighbour == NULL)
		return false;
	else
		return (time(NULL) - neighbour->lastlongHello < 120);
}

// A function which can start a protocol thread. It is assumed that a threaded
// protocol isn't graphical, and should be given a port as argument. Remember
// that due to the number of global variables, there shouldn't be multiple
// protocol threads in the same process. Argument is either NULL (random port)
// or a pointer to an int to use as port.
void* runThreadedProtocol(void *data)
{
    int port;
    if(data == NULL) port = (rand() % (1<<15)) + 1024;
    else port = *((int *) data);
    runProtocol(port, 2, false);
	//printf("Port is : %d\n",port);
    return NULL;
}

void printVP(NodeVP *VP)
{
    if(VP == NULL) return;
    displayError("VP : ");
	char tmpaddr[33];
    for(int i = 0; i < 16; i++) sprintf(tmpaddr+2*i,"%02x", VP->key.addrIP.s6_addr[i]);
    displayError("%s, %hu\n", tmpaddr, VP->key.port);
    printVP(VP->left);
    printVP(VP->right);
}

void printVA(NodeVA *VA)
{
    if(VA == NULL) return;
    displayError("VA : ");
    displayError("%hu, id : %llu, hellos : %d, %d\n", VA->key.port, VA->id, VA->lastHello, VA->lastlongHello);
    printVA(VA->left);
    printVA(VA->right);
}

void runProtocol(int port, int display, bool multicast)
{
	// Initialisation des structures
	srand(seedMask ^ time(NULL));
	id = ((long long)rand()<<32) | (long long) rand();
	displayError("id : %llu, port : %d\n",id,port);
	// Créer socket, initialise aussi la boucle d'écoute pour recevoir des messages
    initSocket(port, multicast);

    time_t hello_timer = time(NULL) - 30;
	time_t neighbour_timer = time(NULL) - 180;

    initDisplay(display);

    protocol_running = true;
	while(protocol_running)
	{
        // Envoyer des Hello et des GoAway
        if(time(NULL) - hello_timer >= 30)
        {
#ifdef DEBUG
            displayError("Sending Hellos\n");
#endif
            pthread_mutex_lock(&mutexVA);
#ifdef DEBUG
	    displayError("TVA size : %d\n",(TVA==NULL?0:TVA->size));
            printVA(TVA);
#endif
            TVA = cleanupNeighbours(TVA);
            int nbsym = sendLongHellos(TVA);
            if(nbsym < minNS)
            {
                // This may be done several times
                if(multicast) sendHelloMulticast();
                NodeVP* neighbour = pickRandVP(TVP); // find a potential neighbour
                if(neighbour != NULL)
                {
#ifdef DEBUG
                    printVP(TVP);
#endif
                    char *tlv = writeShortHello(id);
                    sendtlv(neighbour->key.addrIP, neighbour->key.port, tlv, true);
                }
            }
            pthread_mutex_unlock(&mutexVA);
            hello_timer = time(NULL);
        }

		// Envoyer parfois des Neighbour
		if(time(NULL) - neighbour_timer >= 60)
		{
#ifdef DEBUG
			displayError("Sending Neighbours\n");
#endif
			pthread_mutex_lock(&mutexVA);
			NodeNL* neighbourList = addVS(TVA,time(NULL),NULL);
			propagateNeighbour(neighbourList,neighbourList);
			pthread_mutex_unlock(&mutexVA);
			neighbour_timer = time(NULL);
		}
		
		// Inondation
		pthread_mutex_lock(&mutexData);
        	TRD = floodData(TRD, time(NULL));
	        pthread_mutex_unlock(&mutexData);
		sleep(1);
    }

    // send GoAway to neighbours before quitting
	pthread_mutex_lock(&mutexVA);
	NodeNL* neighbours = addVS(TVA, time(NULL), NULL);
	pthread_mutex_unlock(&mutexVA);
	char* tlv = writeGoAway(1,"I'm stopping");
	sendGoAway(neighbours,tlv);
	free(tlv);
    destroyNL(neighbours);
    closeSocket();
    pthread_mutex_lock(&mutexVA);
	destroyVP(TVP);
	destroyVA(TVA);
    pthread_mutex_unlock(&mutexVA);
    pthread_mutex_lock(&mutexData);
	destroyRD(TRD);
    pthread_mutex_unlock(&mutexData);
	while(displayClosing)
	{
		sleep(0.1);
	}
}
