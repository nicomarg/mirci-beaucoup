#ifndef PROTOCOL_H
#define PROTOCOL_H

#include "communication.h"
#include "messages.h"
#include "treap.h"
#include "display.h"
#include <signal.h>

extern pthread_mutex_t mutexVA;
extern NodeVP* TVP;
extern NodeVA* TVA;
extern pthread_mutex_t mutexData;
extern NodeRD* TRD;
extern unsigned long long id;

void shortHello(unsigned long long, struct sockaddr_in6*);
void longHello(unsigned long long, unsigned long long, struct sockaddr_in6*);
void longHelloMulticast(unsigned long long, unsigned long long, struct sockaddr_in6*);

bool isSymmetrical(NeighbourKey);

void sendMessage(char*);

void printVP(NodeVP*);
void printVA(NodeVA*);

void stopProtocol(void);
void* runThreadedProtocol(void*);
void runProtocol(int,int,bool);

#endif
