#define DEBUG
#include "protocol.h"

struct in6_addr iplocale;

int main()
{
    int port[4];
	for(int i=0;i<4;i++)
		port[i]=rand()%(1<<14)+1024;
	memset(iplocale.s6_addr,0,16);
	iplocale.s6_addr[15]=1;
	if(fork()){
		if(fork()) {
			printf("%d\n",port[0]);
			TVP = insertVP(TVP,createVP(iplocale,port[1]));
			TVP = insertVP(TVP,createVP(iplocale,port[3]));
    		runProtocol(port[0], false);
		}
		else {
			printf("%d\n",port[1]);
			TVP = insertVP(TVP,createVP(iplocale,port[0]));
			TVP = insertVP(TVP,createVP(iplocale,port[2]));
			runProtocol(port[1], false);
		}
	}
	else
	{
		if(fork()) {
			printf("%d\n",port[2]);
			TVP = insertVP(TVP,createVP(iplocale,port[1]));
			TVP = insertVP(TVP,createVP(iplocale,port[3]));
			runProtocol(port[2], false); 
		}
		else {
			printf("%d\n",port[3]);
			TVP = insertVP(TVP,createVP(iplocale,port[0]));
			TVP = insertVP(TVP,createVP(iplocale,port[2]));
			runProtocol(port[3], false);
		}
	}
}
