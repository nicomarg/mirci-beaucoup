#include "protocol.h"

int main(int argc, char *argv[])
{
    char message[50];
    memset(message, 0, 50);
    int port, neighbour;
    struct sockaddr_in6 localhost;
	memset(&localhost, 0, sizeof(localhost));
    /*struct addrinfo info;
    memset(&info, 0, sizeof(info));
    info.ai_socktype = SOCK_DGRAM;
    info.ai_protocol = 0;
    info.ai_flags = AI_V4MAPPED | AI_ALL;
    struct addrinfo *res;
    getaddrinfo("localhost", NULL , &info, &res);
    memcpy(&localhost, res->ai_addr, res->ai_addrlen);
	freeaddrinfo(res);*/
	localhost.sin6_family = AF_INET6;
	localhost.sin6_addr.s6_addr[15]=1;
	if(argc < 3)
    {
        printf("Please provide a port number and an initial neighbour port\n");
        return 0;
    }
    else
    {
        port = atoi(argv[1]);
        neighbour = atoi(argv[2]);
    }
	localhost.sin6_port=port;
	for(unsigned int i=0;i<sizeof(localhost);i++) printf("%x",*((char*)&localhost + i));
	printf("\n");
    pthread_mutex_lock(&mutexVA);
    TVP = insertVP(TVP, createVP(localhost.sin6_addr, neighbour));
    pthread_mutex_unlock(&mutexVA);
    /*pthread_t thread;
    pthread_create(&thread, NULL, runThreadedProtocol, &port);
    while(true)
    {
        sleep(10);
        sprintf(message, "%d: %d", port, nonce);
        printf("Sending %s\n", message);
        fflush(stdout);
        sendMessage(message);
        nonce++;
    }*/
	runProtocol(port,2, false);
}
