#include "protocol.h"

int main(int argc,char** argv)
{
    char message[50];
    memset(message, 0, 50);
	if(argc<1) return 0;
    int port=atoi(argv[1]);
    struct sockaddr_in6 server;
	memset(&server, 0, sizeof(server));
    struct addrinfo info;
    memset(&info, 0, sizeof(info));
    info.ai_socktype = SOCK_DGRAM;
    info.ai_protocol = 0;
    info.ai_flags = AI_V4MAPPED | AI_ALL;
    struct addrinfo *res;
    int ret = getaddrinfo("ynerant.fr", "10042" , &info, &res);
    printf("Return value : %d\n",ret);
	memcpy(&server, res->ai_addr, res->ai_addrlen);
	freeaddrinfo(res);
	server.sin6_family = AF_INET6;
	/*if(argc < 3)
    {
        printf("Please provide a port number and an initial neighbour port\n");
        return 0;
    }
    else
    {
        port = atoi(argv[1]);
        neighbour = atoi(argv[2]);
    }*/
	// server.sin6_port=port;
	for(unsigned int i=0;i<sizeof(server.sin6_addr.s6_addr);i++) printf("%02x",server.sin6_addr.s6_addr[i]);
	printf("\n");
    pthread_mutex_lock(&mutexVA);
    TVP = insertVP(TVP, createVP(server.sin6_addr, 10042));
    pthread_mutex_unlock(&mutexVA);
    /*pthread_t thread;
    pthread_create(&thread, NULL, runThreadedProtocol, &port);
    while(true)
    {
        sleep(10);
        sprintf(message, "%d: %d", port, nonce);
        printf("Sending %s\n", message);
        fflush(stdout);
        sendMessage(message);
        nonce++;
    }*/
	runProtocol(port,2,false);
}
