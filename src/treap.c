#include "treap.h"

#define card(u) (u==NULL?0:u->size)

#define ARBRE(TYPE,PTYPE,KTYPE,KCMP,KEQ,DESTROYFN,CUTFN,MERGEFN, \
FINDFN,INSERTFN,REMOVEFN,FINDKTHFN,FREEFN) \
\
typedef struct PTYPE {\
	TYPE *first,*second;\
} PTYPE;\
\
void DESTROYFN(TYPE* u) {\
	if(u==NULL) return;\
	DESTROYFN(u->left);\
	DESTROYFN(u->right);\
	FREEFN(u);\
}\
\
PTYPE CUTFN(TYPE* u, KTYPE x) {\
	PTYPE out={NULL,NULL};\
	if(u==NULL) return out; \
	if(KCMP(u->key,x)) {\
		PTYPE rc=CUTFN(u->right,x);\
		u->right=rc.first;\
		u->size-=card(rc.second);\
		out.first=u, out.second=rc.second;\
		return out;\
	} else {\
		PTYPE lc=CUTFN(u->left,x);\
		u->left=lc.second;\
		u->size-=card(lc.first);\
		out.first=lc.first, out.second=u;\
		return out;\
	}\
}\
\
TYPE* MERGEFN(TYPE* u, TYPE* v) {\
	if(u==NULL) return v;\
	if(v==NULL) return u;\
	if(u->rank<v->rank) {\
		u->size+=v->size;\
		u->right=MERGEFN(u->right,v);\
		return u;\
	} else {\
		v->size+=u->size;\
		v->left=MERGEFN(u,v->left);\
		return v;\
	}\
}\
\
TYPE* INSERTFN(TYPE* u, TYPE* v) {\
	PTYPE cuts=CUTFN(u,v->key);\
	return MERGEFN(cuts.first,MERGEFN(v,cuts.second));\
}\
\
TYPE* FINDFN(TYPE* u, KTYPE x) {\
	if(u==NULL || KEQ(u->key,x)) return u;\
	else if(KCMP(u->key,x)) return FINDFN(u->right,x);\
	else return FINDFN(u->left,x);\
}\
\
TYPE* REMOVEFN(TYPE* u, KTYPE x) {\
	if(u==NULL) return NULL;\
	else if(KEQ(u->key,x)) {\
		TYPE *uleft=u->left, *uright=u->right;\
		free(u);\
		return MERGEFN(uleft,uright);\
	} else if(KCMP(u->key,x)) {\
		u->right=REMOVEFN(u->right,x);\
		u->size=1+card(u->left)+card(u->right);\
		return u;\
	} else {\
		u->left=REMOVEFN(u->left,x);\
		u->size=1+card(u->left)+card(u->right);\
		return u;\
	}\
}\
\
TYPE* FINDKTHFN(TYPE* u, int k) {\
	int leftSz=card(u->left);\
	if(k<leftSz) {\
		return FINDKTHFN(u->left,k);\
	} else if(k==leftSz) {\
		return u;\
	} else {\
		return FINDKTHFN(u->right,k-leftSz-1);\
	}\
}

NeighbourKey addrtokey(struct sockaddr_in6 addr)
{
    NeighbourKey ret;
    memcpy(&ret.addrIP, &addr.sin6_addr, 16);
    ret.port = ntohs(addr.sin6_port);
    return ret;
}

bool neighKeyEq(NeighbourKey a, NeighbourKey b)
{
    bool equal = true;
    for (int i = 0; equal && i < 16; i++) equal = equal && a.addrIP.s6_addr[i] == b.addrIP.s6_addr[i];
	return equal && a.port == b.port;
}

bool neighKeyComp(NeighbourKey a,NeighbourKey b)
{
    bool lt = false;
    bool equal = true;
    for (int i = 0; i < 16; i++)
    {
        if (a.addrIP.s6_addr[i] < b.addrIP.s6_addr[i])
        {
            lt = true;
            equal = false;
            break;
        }
        if (a.addrIP.s6_addr[i] != b.addrIP.s6_addr[i])
        {
            equal = false;
            break;
        }
    }
	return lt || (equal && a.port < b.port);
}

ARBRE(NodeVP,PNodeVP,NeighbourKey,neighKeyComp,neighKeyEq,destroyVP,cutVP,mergeVP,
findVP,insertVP,removeVP,findkthVP,free)

ARBRE(NodeVA,PNodeVA,NeighbourKey,neighKeyComp,neighKeyEq,destroyVA,cutVA,mergeVA,
findVA,insertVA,removeVA,findkthVA,free)

void initVP(NodeVP* u, struct in6_addr addrIP, short port, int r)
{
    u->key.addrIP=addrIP; u->key.port=port; u->rank=r; u->left=NULL; u->right=NULL;
	u->size=1;
}

NodeVP* createVP(struct in6_addr addrIP, short port)
{
	NodeVP *v = (NodeVP*)malloc(sizeof(NodeVP));
	initVP(v,addrIP,port,rand());
    return v;
}

NodeVP* pickRandVP(NodeVP* u)
{
    if(u == NULL)return NULL;
	int n = u->size;
	int k = rand()%n;
	return findkthVP(u,k);
}

void initVA(NodeVA* u, struct in6_addr addrIP, short port, int r)
{
    u->key.addrIP=addrIP; u->key.port=port; u->rank=r; u->left=NULL; u->right=NULL;
	u->size=1;
}

NodeVA* createVA(struct in6_addr addrIP, short port)
{
	NodeVA *v = (NodeVA*)malloc(sizeof(NodeVA));
	initVA(v,addrIP,port,rand());
    return v;
}

bool dataKeyEq(DataKey a, DataKey b)
{
	return a.id==b.id && a.nonce==b.nonce;
}

bool dataKeyComp(DataKey a, DataKey b)
{
	return a.id<b.id || (a.id==b.id && a.nonce<b.nonce);
}

NodeNL* createNL(NeighbourKey key, time_t now)
{
	NodeNL* u = (NodeNL*)malloc(sizeof(NodeNL));
	u->key=key; u->rank=rand(); u->left=NULL; u->right=NULL;
	u->size=1;
	u->cnt=0;
	u->lastSend=now; u->waitingTime=0;
	return u;
}

NodeNL* addVS(NodeVA* u, time_t now, NodeNL* l) // l is NULL for first call
{
	if(u==NULL) return l;
	if(now-(u->lastlongHello)<120)
	{
		NodeNL* v = createNL(u->key,now);
		l=insertNL(l,v);
	}
	l=addVS(u->left,now,l);
	l=addVS(u->right,now,l);
	return l;
}

ARBRE(NodeNL,PNodeNL,NeighbourKey,neighKeyComp,neighKeyEq,destroyNL,cutNL,mergeNL,
findNL,insertNL,removeNL,findkthNL,free)

ARBRE(NodeRD,PNodeRD,DataKey,dataKeyComp,dataKeyEq,destroyRD,cutRD,mergeRD,
findRD,insertRD,removeRD,findkthRD,freeRD)

void freeRD(NodeRD* u)
{
	free(u->data);
	destroyNL(u->neighbourList);
	free(u);
}

void initRD(NodeRD* u, unsigned long long id, unsigned long nonce, char* data, int rank)
{
	u->key.id=id; u->key.nonce=nonce; u->rank=rank; u->data=data; u->left=NULL; u->right=NULL;
	u->date=time(NULL);
	u->size=1;
}

NodeRD* createRD(unsigned long long id, unsigned long nonce, char* data)
{
	NodeRD *v = (NodeRD*)malloc(sizeof(NodeRD));
	initRD(v,id,nonce,data,rand());
	return v;
}
