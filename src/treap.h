#ifndef TREAP_H
#define TREAP_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <time.h>

typedef struct NeighbourKey
{
    struct in6_addr addrIP;
	unsigned short port;
} NeighbourKey;

NeighbourKey addrtokey(struct sockaddr_in6);

bool neighKeyEq(NeighbourKey,NeighbourKey);

bool neighkeyComp(NeighbourKey,NeighbourKey);

typedef struct NodeVP
{
    NeighbourKey key;
    int rank,size;
    struct NodeVP *left, *right;
} NodeVP;

void initVP(NodeVP*,struct in6_addr,short,int);

NodeVP* createVP(struct in6_addr,short);

void destroyVP(NodeVP*);

NodeVP* mergeVP(NodeVP*,NodeVP*);

NodeVP* findVP(NodeVP*,NeighbourKey);

NodeVP* insertVP(NodeVP*,NodeVP*);

NodeVP* removeVP(NodeVP*,NeighbourKey);

NodeVP* findkthVP(NodeVP*,int);

NodeVP* pickRandVP(NodeVP*);

typedef struct NodeVA
{
	NeighbourKey key;
	int rank,size;
	unsigned long long id;
	time_t lastHello, lastlongHello;
	struct NodeVA *left, *right;
} NodeVA;

void initVA(NodeVA*,struct in6_addr,short,int);

NodeVA* createVA(struct in6_addr,short);

void destroyVA(NodeVA*);

NodeVA* mergeVA(NodeVA*,NodeVA*);

NodeVA* findVA(NodeVA*,NeighbourKey);

NodeVA* insertVA(NodeVA*,NodeVA*);

NodeVA* removeVA(NodeVA*,NeighbourKey);

NodeVA* findkthVA(NodeVA*,int);

typedef struct DataKey
{
	unsigned long long id;
	unsigned long nonce;
} DataKey;

bool dataKeyEq(DataKey,DataKey);

bool dataKeyComp(DataKey,DataKey);

typedef struct NodeNL
{
	NeighbourKey key;
	int rank,size;
	unsigned char cnt;
	time_t lastSend,waitingTime;
	struct NodeNL *left,*right;
} NodeNL;

NodeNL* createNL(NeighbourKey,time_t);

void destroyNL(NodeNL*);

NodeNL* mergeNL(NodeNL*,NodeNL*);

NodeNL* findNL(NodeNL*,NeighbourKey);

NodeNL* insertNL(NodeNL*,NodeNL*);

NodeNL* removeNL(NodeNL*,NeighbourKey);

NodeNL* addVS(NodeVA*, time_t, NodeNL*);

typedef struct NodeRD
{
	DataKey key;
	int rank,size;
	char* data;
	NodeNL* neighbourList;
	time_t date;
	struct NodeRD *left,*right;
} NodeRD;

void initRD(NodeRD*, unsigned long long, unsigned long, char*, int);

NodeRD* createRD(unsigned long long, unsigned long, char*);

void freeRD(NodeRD*);

void destroyRD(NodeRD*);

NodeRD* mergeRD(NodeRD*,NodeRD*);

NodeRD* findRD(NodeRD*,DataKey);

NodeRD* insertRD(NodeRD*,NodeRD*);

NodeRD* removeRD(NodeRD*,DataKey);

#endif
